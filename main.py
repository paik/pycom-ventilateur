from time import sleep
try:
    from pycom import heartbeat, rgbled, nvs_get, nvs_set
    pycom_board = True
except:
    pycom_board = False
from temp import Temp
from http_server import HTTP_SERVER
from utime import ticks_ms
from wifi import W
from config import add_exception, start_thread
from gc import mem_alloc, mem_free, collect

"""
from dht import DTH
from machine import Pin

if pycom_board:
    heartbeat(False)
    #rgbled(0x000008) # blue
th = DTH(Pin(Pin.exp_board.GPIO2, mode=Pin.OPEN_DRAIN),1)
sleep(2)
result = th.read()
if result.is_valid():
    #if pycom_board:
        #rgbled(0x001000) # green
    print('Temperature: {:3.2f}'.format(result.temperature/1.0))
    print('Humidity: {:3.2f}'.format(result.humidity/1.0))
"""

# Set red light at start
if pycom_board:
    heartbeat(False)
    rgbled(0x7f0000)
    try:
        errors = nvs_get('errors')
    except:
        print("Erreurs depuis la NVS")
        nvs_set('errors', "")
add_exception(ticks_ms(), "Starting main...", True)
sleep(1)
thread_temp = start_thread(Temp())
HTTP_SERVER = start_thread(HTTP_SERVER(thread_temp))
w = start_thread(W())

print("\rDémarré !")
if pycom_board:
    rgbled(False)
sleep(1)
