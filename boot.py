#!/usr/bin/env python
from os import uname
try:
    from pycom import wifi_on_boot
    wifi_on_boot(False)
    from network import Bluetooth
    Bluetooth().deinit()
except:
    pass


print("\rDémarrage...")

print(uname())
