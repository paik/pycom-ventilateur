from machine import Pin
from lib.onewire import DS18X20
from lib.onewire import OneWire
from utime import ticks_ms
from time import sleep
from config import version, add_exception, config_editable, ms_to_date_string #, etat
from math import fabs
try:
    from pycom import nvs_get, nvs_set
    pycom_board = True
except:
    pycom_board = False
from gc import collect


class Temp(object):
    def __init__(self):

        self.version = version
        self.state = 0
        self.state_old = ""
        self.dernier_chrono = 0
        self.start = ticks_ms()
        self.t = None
        try:
            if pycom_board:
                self.wait = nvs_get('wait')
            else:
                self.set_wait()
            if self.wait > ticks_ms() + 5 * 60 * 60 * 1000:
                self.set_wait()
        except:
            print("Erreurs depuis la NVS")
            self.set_wait()
        try:
            if pycom_board:
                self.mode = nvs_get('mode')
            else:
                self.set_mode()
        except:
            print("Erreurs depuis la NVS")
            self.set_mode()
        self.stop = 0

        self.refresh_config()

        self.relay = Pin(config_editable["pin_relais"], mode=Pin.OUT, value=0)
        #self.ow = OneWire(Pin('P10'))
        self.ow = OneWire(Pin(config_editable["pin_ow"]))
        self.temp = None
        #self.connect()

        add_exception(ticks_ms(), "Starting temp")

    def refresh_config(self):
        self.attente_inter_boucle = config_editable["attente_inter_boucle"]
        self.seuil_allumage = config_editable["seuil_allumage"]
        self.seuil_extinction = config_editable["seuil_extinction"]

    def set_wait(self, t=ticks_ms()):
        self.wait = t
        if pycom_board:
            nvs_set('wait', self.wait)

    def set_mode(self, m=0):
        self.mode = m
        if pycom_board:
            nvs_set('mode', self.mode)

    def connect(self):
        if config_editable["debug"]:
            print("Connecting to DS18X20...")
        if len(self.ow.scan()) and self.temp is None:
            self.temp = DS18X20(self.ow)

    def is_connected(self):
        if len(self.ow.scan()) and self.temp is not None and len(self.temp.roms) and self.temp.roms[0] is not None:
            #print("DS18X20 connected")
            return True
        else:
            if config_editable["debug"]:
                print("DS18X20 NOT connected !")
            return False

    def change_state(self, s):
        self.dernier_chrono = ticks_ms() - self.start
        self.state_old = self.state
        self.state = s
        self.start = ticks_ms()

    def allumer(self):
        self.change_state(1)
        print("Chrono : " + str(ms_to_date_string(self.dernier_chrono)))
        self.relay.value(1)
        print("On allume le ventilateur")
        add_exception(ticks_ms(), "Allumage : " + str(self.t) + " °C")

    def eteindre(self):
        self.change_state(-1)
        print("Chrono : " + str(ms_to_date_string(self.dernier_chrono)))
        self.relay.value(0)
        print("On éteint le ventilateur")
        add_exception(ticks_ms(), "Extinction : " + str(self.t) + " °C")

    def get_temp(self):
        i = 0
        diff = 20.0
        limit = 10.0
        if self.temp is None:
            return None
        while diff > limit and i < 5:
            sleep(1)
            self.temp.start_conversion()
            sleep(1)
            t1 = self.temp.read_temp_async()
            sleep(1)
            self.temp.start_conversion()
            sleep(1)
            t2 = self.temp.read_temp_async()
            i += 1
            try:
                diff = fabs(t2 - t1)
                if t1 == -0.0625 or t1 < -55.0 or t1 > 125.0:
                    diff = limit + 1
                    i -= 1
            except:
                diff = limit + 1
        return t1

    def refresh_temp(self):
        if not self.is_connected() and not self.is_connected():
            if self.state != 0:
                add_exception(ticks_ms(), "DS18X20 pas connecté - Etat inconnu")
            self.change_state(0)
            sleep(1)
            self.connect()
            return
        self.t = self.get_temp()

    def run(self):
        while True and not self.stop:
            try:
                collect()
                self.refresh_temp()
                if self.mode == 0 and ticks_ms() > self.wait:
                    self.loop()
                    sleep(1)
                else:
                    sleep(1)
            except:
                sleep(1)

    def loop(self):
        try:
            self.refresh_config()
            if self.t and self.t >= self.seuil_allumage and self.state != 1:
                self.allumer()
            elif self.t and self.t <= self.seuil_extinction and self.state != -1:
                self.eteindre()
            '''
            print("Version : " + str(self.version))
            print("PySense allumé depuis " + str(ms_to_date_string(ticks_ms())))
            print("Attente inter boucle : " + str(self.attente_inter_boucle) + " sec")
            print("Température = " + str(self.t))
            print("DS18X20 : " + str(self.temp.roms))
            print("scan : " + str(self.ow.scan()))
            print("Etat = " + str(etat[self.state]) + " depuis : " + str(ms_to_date_string((ticks_ms() - self.start))))
            #print("Messages : " + str(messages))
            print("\r\n")
            '''
            sleep(self.attente_inter_boucle)
        except Exception as e:
            add_exception(ticks_ms(), "Temp error : " + str(e), True)
