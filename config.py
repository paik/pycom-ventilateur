from math import isnan, trunc, isfinite
from utime import localtime, ticks_ms
from _thread import start_new_thread
try:
    from pycom import nvs_get, nvs_set
    from network.WLAN import STA, WPA2
    pycom_board = True
except:
    pycom_board = False
    from network import STA_IF as STA, AUTH_WPA_WPA2_PSK as WPA2
from machine import RTC, reset

rtc = RTC()
if pycom_board:
    rtc.ntp_sync("pool.ntp.org")

version = 2.0
mode = {0:'auto', 1:'manuel'}
etat = {0:"inconnu", 1:"allumé", -1:"éteint"}
messages = {}

config_editable = {
    "seuil_allumage": 35,
    "seuil_extinction": 30,
    "attente_inter_boucle": 10,
    "debug": True,
}
if pycom_board:
    config_editable["pin_ow"] = Pin.exp_board.GPIO2
    config_editable["pin_relais"] = 'P22'
else:
    config_editable["pin_ow"] = 23
    config_editable["pin_relais"] = 22

config_wifi = {
    "wifi_mode": STA,
    "wifi_sta": {
    'B': (WPA2, "cotcotcotcot"),
    'BuenosAyres': None,
    },
    "wifi_ssid_ap": 'P',
    "wifi_auth_ap": (WPA2, "cotcotcotcot"),
}

def add_exception(t, e, nvs=False):
    if not pycom_board:
        nvs = False
    print(e)
    #return
    global messages
    i = len(messages)
    try:
        if i > 300:
            messages = {}
            i = len(messages)
        #messages[i+1] = {'date' : rtc_to_str(rtc_now(rtc)), 'time' : ms_to_date_string(t), 'text' : e}
    except:
        messages = {}
        i = len(messages)
    if pycom_board and rtc.synced():
        messages[i+1] = {'time' : rtc_to_str(rtc_now(rtc)), 'text' : e}
    else:
        messages[i+1] = {'time' : ms_to_date_string(t), 'text' : e}
    if nvs:
        try:
            errors = nvs_get('errors')
            errors += rtc_to_str(rtc_now(rtc)) + "-" + ms_to_date_string(t) + ' : ' + e + ', '
            if len(errors) >= 1984:
                errors = rtc_to_str(rtc_now(rtc)) + "-" + ms_to_date_string(t) + " : Reset NVS,"
                errors += rtc_to_str(rtc_now(rtc)) + "-" + ms_to_date_string(t) + ' : ' + e + ', '
            nvs_set('errors', errors)
        except:
            errors = rtc_to_str(rtc_now(rtc)) + "-" + ms_to_date_string(t) + " : Excepton - Reset NVS,"
            errors += rtc_to_str(rtc_now(rtc)) + "-" + ms_to_date_string(t) + ' : ' + e + ', '
            nvs_set('errors', errors)

def rtc_now(rtc):
    if hasattr(rtc, 'now'):
        return rtc.now()
    else:
        return rtc.datetime()

def rtc_to_str(now):
    return str(now[0]) + "-" + str(now[1]) + "-" + str(now[2]) + " " + str(now[3]) + ":" + str(now[4]) + ":" + str(now[5])

def ms_to_date_string(ms):
    s = ""
    if not isnan(ms) and isfinite(ms):
        t = localtime(trunc(ms/1000.0))
        if t[7] > 1:
            s += str(t[7] - 1) + " jours " + str(t[3]) + " heures " + str(t[4]) + " min " + str(t[5]) + " sec"
        elif t[3] > 0:
            s += str(t[3]) + " heures " + str(t[4]) + " min " + str(t[5]) + " sec"
        elif t[4] > 0:
            s += str(t[4]) + " min " + str(t[5]) + " sec"
        else:
            s += str(t[5]) + " sec"
        return s
    return None

def start_thread(t, a=None):
    if a is None:
        start_new_thread(t.run, ())
    else:
        start_new_thread(t.run, (a))
    return t

def generer_form(dict, m=-1, prefix=None):
    message = '<form action="/changerConfig">'
    for c in dict:
        message += '<label for="' + str(c) + '">' + str(c) + ':</label>'
        if prefix is None:
            message += generer_type(dict[c], c)
        else:
            message += generer_type(dict[c], str(prefix) + "_" + str(c))
    if m > -1:
        #message += '<input type="hidden" name=moteurId id=moteurId value="' + str(m) + '">'
        message += '<input type="reset" value="Reset"><input type="submit" value="Submit"> </form> <br>'
    return message

def generer_type(value, name):
    message = ''
    if type(value)==float:
        message += '<input type="number" required step="0.1" id="' + str(name) + '" name="' + str(name) + '" value=' + str(value) + '><br><br>'
    elif type(value)==int and str(name)=="couleur":
        message += '<input type="color" required id="' + str(name) + '" name="' + str(name) + '" value=#' + '{:0>6}'.format(str(hex(value))[2:]) + '><br><br>'
    elif type(value)==int:
        message += '<input type="number" required step="1" id="' + str(name) + '" name="' + str(name) + '" value=' + str(value) + '><br><br>'
    elif type(value)==bool:
        if value:
            message += '<br><input type="radio" checked id="' + str(name) + '-true" name="' + str(name) + '" value=1>'
            message += '<label for="'+ str(name) + 'true">Oui</label><br>'
            message += '<input type="radio" id="' + str(name) + '-false" name="' + str(name) + '" value=0>'
            message += '<label for="'+ str(name) + 'false">Non</label><br><br>'
        else:
            message += '<br><input type="radio" id="' + str(name) + '-true" name="' + str(name) + '" value=1>'
            message += '<label for="'+ str(name) + 'true">Oui</label><br>'
            message += '<input type="radio" checked id="' + str(name) + '-false" name="' + str(name) + '" value=0>'
            message += '<label for="'+ str(name) + 'false">Non</label><br><br>'
    elif type(value)==str:
        message += '<input type="text" required readonly id="' + str(name) + '" name="' + str(name) + '" value=' + str(value) + '><br><br>'
    #elif type(value)==Pin:
    #    message += '<input type="text" required readonly id="' + str(name) + '" name="' + str(name) + '" value=' + str(value).replace(" ","") + '><br><br>'
    elif type(value)==dict:
        message += generer_form(value, prefix=name)
    else:
        print("autre type pour " + str(name) + " : " + str(type(value)))
        print(value)
        message += '<input type="text" required readonly id="' + str(name) + '" name="' + str(name) + '" value=' + str(value) + '><br><br>'
    return message

def response(r, t, n):
    message = "HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=utf-8\r\nCache-Control: no-cache, no-store, must-revalidate\r\nPragma: no-cache\r\nExpires: 0\r\nConnection:close \r\n\r\n" #HTTP response
    '''
    head = '<head><meta name="viewport" content="width=device-width, initial-scale=1">'
    head += '<style>.switch {position: relative;  display: inline-block;  width: 60px;  height: 34px;}'
    head += '.switch input {   opacity: 0;  width: 0;  height: 0;}'
    head += '.slider {  position: absolute;  cursor: pointer;  top: 0;  left: 0;  right: 0;  bottom: 0;  background-color: #ccc;  -webkit-transition: .4s;  transition: .4s;}'
    head += '.slider:before {  position: absolute;  content: "";  height: 26px;  width: 26px;  left: 4px;  bottom: 4px;  background-color: white;  -webkit-transition: .4s;  transition: .4s;}'
    head += 'input:checked + .slider {  background-color: #2196F3;}'
    head += 'input:focus + .slider {  box-shadow: 0 0 1px #2196F3;}'
    head += 'input:checked + .slider:before {  -webkit-transform: translateX(26px);  -ms-transform: translateX(26px);  transform: translateX(26px);}'
    head += '.slider.round {  border-radius: 34px;}'
    head += '.slider.round:before {  border-radius: 50%;}'
    head += '</style></head>'
    '''
    head = ""

    if "GET / " in str(r):
        #this is a get response for the page
        # Sends back some data
        message += "<html>"
        #message += head
        message += "<body><h1> Ventilo ("+ str(n) + ")</h1><br>"
        message += "<a href='/auto'><button>Mode auto</button></a><br>"
        message += "<a href='/manuel'><button>Mode manuel</button></a><br>"
        message += "<a href='/allumer'><button>Allumer</button></a><br>"
        message += "<a href='/eteindre'><button>Eteindre</button></a><br>"
        message += "<a href='/5h_eteindre'><button>Eteindre pour 5 heures</button></a><br>"
        message += "<a href='/infos'><button>Infos</button></a><br>"
        #message += "Test <label class='switch'><input type='checkbox'><span class='slider round'></span></label> a<br>"
        message += "Etat = " + str(etat[t.state]) + " depuis : " + str(ms_to_date_string((ticks_ms() - t.start))) + "<br>"
        message += "Température : " + str(t.t) + " °C<br>"
        message += "Mode : " + str(mode[t.mode]) + "<br>"
        message += "Attente avant mode normal : " + str(ms_to_date_string(max(0, (t.wait - ticks_ms())))) + " s<br>"
        message += "</body></html>"
    elif "GET /request_data" in str(r):
        message = "HTTP/1.1 200 OK\r\nContent-Type: application/json; charset=utf-8\r\nCache-Control: no-cache, no-store, must-revalidate\r\nPragma: no-cache\r\nExpires: 0\r\nConnection:close \r\n\r\n" #HTTP response
        temp = t.get_temp()
        if temp is None:
            message += '{"temperature": null }'
        else:
            message += '{"temperature": ' + str(temp) + ' }'
    elif "GET /infos" in str(r):
        message += "<html><body><h1> Ventilo ("+ str(n) + ")</h1><br>"
        message += "<a href='/request_data'><button>Request data</button></a><br>"
        message += "<a href='/reset'><button>Reset</button></a><br>"
        message += "<a href='/nvsmessages'><button>NVS et messages</button></a><br>"
        message += "<a href='/stop'><button>Stop</button></a><br>"
        message += "<a href='/'><button>Retour</button></a><br>"
        message += "Version : " + str(t.version) + "<br>"
        message += "PySense allumé depuis " + str(ms_to_date_string(ticks_ms())) + "<br>"
        message += "Attente inter boucle : " + str(t.attente_inter_boucle) + " sec<br>"
        message += "Etat = " + str(etat[t.state]) + " depuis : " + str(ms_to_date_string((ticks_ms() - t.start))) + "<br>"
        message += "Précédemment " + str(etat[t.state_old]) + " depuis : " + str(ms_to_date_string(t.dernier_chrono)) + "<br>"
        message += generer_form(config_editable, 0)
        message += "</body></html>"
    elif "GET /nvsmessages " in str(r):
        message += "<html><body><h1> Ventilo ("+ str(n) + ")</h1><br>"
        message += "<a href='/erase'><button>Erase NVS</button></a><br>"
        message += "<a href='/'><button>Retour</button></a><br>"
        message += "NVS errors : " + "<br>"
        if pycom_board:
            for k in nvs_get('errors').split(','):
                message += k + "<br>"
        message += "Messages :<br>"
        j = len(messages)
        for i in messages:
            if j < len(messages) - 200:
                message += "Messages truncated (more than 200)<br>"
                break
            #message += str(j) + " - " + str(messages[j]['date']) + " - " + str(messages[j]['time']) + " : " + str(messages[j]['text']) + "<br>"
            message += str(j) + " - " + str(messages[j]['time']) + " : " + str(messages[j]['text']) + "<br>"
            j -= 1
        message += "</body></html>"
    elif "GET /auto" in str(r):
        t.set_wait()
        t.set_mode(0)
        add_exception(ticks_ms(), "Mode auto")
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /manuel" in str(r):
        t.set_wait()
        t.set_mode(1)
        add_exception(ticks_ms(), "Mode manuel")
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /allumer" in str(r):
        t.allumer()
        t.set_wait()
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /eteindre" in str(r):
        t.eteindre()
        t.set_wait()
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /5h_eteindre" in str(r):
        t.eteindre()
        t.set_wait(ticks_ms() + 5 * 60 * 60 * 1000)
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /stop " in str(r):
        t.stop = 1
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /erase " in str(r):
        if pycom_board:
            nvs_set('errors', "")
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /reset " in str(r):
        reset()
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /changerConfig" in str(r):
        try:
            configs=str(r).split(" ")[1].split("?")[1].split("&")
            out={}
            for c in configs:
                config = c.split("=")
                out[str(config[0])] = str(config[1])
            for c in config_editable:
                if type(config_editable[c])==float:
                    config_editable[c]=float(out[c])
                elif type(config_editable[c])==int and str(c)=="couleur":
                    config_editable[c]=int('0x' + str(out[c])[3:])
                elif type(config_editable[c])==int:
                    config_editable[c]=int(out[c])
                elif type(config_editable[c])==bool:
                    config_editable[c]=bool(int(out[c]))
                elif type(config_editable[c])==str:
                    config_editable[c]=str(out[c])
                elif type(config_editable[c])==dict:
                    for c2 in config_editable[c]:
                        if type(config_editable[c][c2])==int:
                            config_editable[c][c2]=int(out[str(c) + "_" + str(c2)])
        except Exception as e:
            add_exception(ticks_ms(), e, True)
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/infos \r\n\r\n"
    elif "GET /" in str(r):
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"

    return message
