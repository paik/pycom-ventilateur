from network import WLAN
from machine import idle, reset
from time import sleep
from config import config_wifi, config_editable, add_exception
from gc import mem_free, mem_alloc
from utime import ticks_ms
try:
    from pycom import rgbled
    from network.WLAN import AP, STA
    pycom_board = True
except:
    pycom_board = False
    from network import AP_IF as AP, STA_IF as STA


# Setting up the wifi
class W(object):

    def __init__(self):
        self.wlan = None

    def check(self):
        if self.wlan is not None and (self.wlan.isconnected() or config_wifi["wifi_mode"] == AP):
            if config_editable["debug"]:
                if pycom_board and self.wlan.ifconfig(id=config_wifi["wifi_mode"]-1)[0] != "0.0.0.0":
                    print("wlan already connected")
                elif not pycom_board and self.wlan.ifconfig()[0] != "0.0.0.0":
                    print("wlan already connected")
        else:
            if config_editable["debug"]:
                if pycom_board:
                    rgbled(0xffff00)
                print("wlan NOT connected")
                sleep(1)
                if pycom_board:
                    rgbled(False)
            try:
                WLAN().disconnect()
            except OSError:
                pass
            if pycom_board:
                WLAN().deinit()
            sleep(1)
            if config_wifi["wifi_mode"] == STA:
                if pycom_board:
                    self.wlan = WLAN(mode=config_wifi["wifi_mode"])
                else:
                    self.wlan = WLAN(config_wifi["wifi_mode"])
                    self.wlan.active(True)
                nets = self.wlan.scan()
                if config_editable["debug"]:
                    print("Networks scanned : %s" % nets)
                for ssid in config_wifi["wifi_sta"]:
                    if config_editable["debug"]:
                        print("Connect to %s with pwd %s" % (ssid, config_wifi["wifi_sta"][ssid]))
                    if pycom_board:
                        self.wlan.connect(ssid=ssid, auth=config_wifi["wifi_sta"][ssid])
                    else:
                        if config_wifi["wifi_sta"][ssid] is not None:
                            self.wlan.connect(ssid, config_wifi["wifi_sta"][ssid][1])
                        else:
                            self.wlan.connect(ssid)
                    i = 0
                    while not self.wlan.isconnected() and i < 5:
                        i += 1
                        if config_editable["debug"]:
                            print("Trying to connect to wifi %s ... %s" % (ssid, i))
                        idle()
                        sleep(1)
                    if self.wlan.isconnected() and config_editable["debug"]:
                        print("WiFi connected succesfully")
                        print(self.wlan.ifconfig())
                        break
                    elif config_editable["debug"]:
                        print("WiFi %s not connected" % ssid)
            elif config_wifi["wifi_mode"] == AP:
                self.wlan = WLAN()
                self.wlan.init(mode=config_wifi["wifi_mode"], ssid=config_wifi["wifi_ssid_ap"], auth=config_wifi["wifi_auth_ap"])
                sleep(2)
            if pycom_board:
                if self.wlan.ifconfig(id=config_wifi["wifi_mode"]-1)[0] == "0.0.0.0":
                    self.wlan = None
                else:
                    add_exception(ticks_ms(), "Wifi connected, ip :" + str(self.wlan.ifconfig(id=config_wifi["wifi_mode"]-1)[0]), True)
            else:
                if self.wlan.ifconfig()[0] == "0.0.0.0":
                    self.wlan = None
                else:
                    add_exception(ticks_ms(), "Wifi connected, ip :" + str(self.wlan.ifconfig()[0]), True)

        return self.wlan

    def loop(self, i = 0):
        try:
            self.check()
        except Exception as e:
            ee = "wifi loop error : " + str(e) + " - mem_alloc = " + str(mem_alloc()) + " - mem_free = " + str(mem_free())
            add_exception(ticks_ms(), ee, True)
            print(self.wlan)
            #self.check()
            i += 1
        sleep(10)
        return i

    def run(self):
        i = 0
        while True:
            i = self.loop(i)
            #print("ping errors : " + str(i))
            if i > 5:
                add_exception(ticks_ms(), "5 wifi check fails, resetting board", True)
                reset()
